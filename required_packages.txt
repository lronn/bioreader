cpan
cpan App::cpanminus

Perl GetOpt::Long
cpanm GetOpt::Long

Perl EUtilities
cpanm Bio::DB::EUtilities

R version 3.2.2

R packages
optparse
tm
SnowballC
genefilter
RTextTools
e1071
ggplot2
rmarkdown
DT
gridExtra
knitr

Pandoc
https://github.com/jgm/pandoc/releases/tag/1.15.0.6




Install in R:
install.packages("DT", repos="http://cran.rstudio.com/")





