#!/usr/bin/perl

use DBI;
use CGI::Lite;
use strict;
use warnings;


############################
### Database connections ###
############################

sub db_connect {
	my $databaseName = 'krdav_private';
	my $dbuser = 'krdav';
	my $dbpassword = 'uUlZSoQm';

	# Make connection to server:
	my $dbc = DBI->connect("DBI:mysql:database=$databaseName;host=mysql.cbs.dtu.dk", $dbuser, $dbpassword, 
		{RaiseError => 1, AutoCommit => 1});
	return ($dbc);
}


my $dbh = db_connect ();




########################################
### Find duplicates in given dataset ###
########################################

my $filename = 'main_table_DB.csv';

open ("IN", "<", $filename) or die $!;
my $header = <IN>;


my %seen;
my $dup = 0;
while (defined(my $line = <IN>)) {
	chomp $line;
	my @el = split("\t", $line);
	my $id = $el[0];
	unless (exists $seen{$id}) {
		$seen{$id}++;
		next;		
	}
    print "'$id' is duplicated.\n";
    $dup++;
}
close IN;
die "Please remove duplicates" if $dup;





##################################################
### Find those ID's that are already in the DB ###
##################################################


my $dbh_main = $dbh->prepare ("SELECT * FROM biomarker_main") or die ("Could not prepare statement: " . $dbh->errstr);
$dbh_main->execute ();


my %there;
while (my $main = $dbh_main->fetchrow_hashref ()) {
	unless (exists $there{$main->{'ENSG_ID'}}) {
		$there{$main->{'ENSG_ID'}} = 1;
	} else {
		die "There seems to be a duplicate ID in the database please check this out! ID = $main->{'ENSG_ID'}";
	}
}



my %update;
my %insert;

foreach my $k (keys %seen) {
	#print "$seen{$k}\n";
	unless (exists $there{$k}) {
		$insert{$k} = 1;
		#print "This will be inserted: $k\n";
		delete $there{$k};
	} else {
		$update{$k} = 1;
	}
}

#if (%there) {
#	foreach my $k (keys %there) {
#		print "This will be deleted: $k\n";
#	}
#}


#foreach my $k (keys %insert) {
#	print "$k\n";
#} 





my $mainUP = $dbh->prepare("UPDATE biomarker_main SET 
TYPE = ?,
oldTYPE = ?,
gene_name = ?,
criterion = ?,
source = ?,
HPA = ?,
rank_baseline = ?,
rank_adipose_subc = ?,
rank_adipose_visc = ?,
rank_blood = ?,
rank_liver = ?,
rank_pancreatic = ?,
rank_skelmus = ?,
extracel_pred = ?,
extracel_lit = ?,
extracel_pred_lit = ?,
extracel_man_cur = ?,
mem_pred = ?,
mem_lit = ?,
mem_pred_lit = ?,
lys_pot = ?,
lys_blast = ?,
signalP = ?,
secretomeP = ?,
in_plasma = ?,
f_score = ? 
WHERE ENSG_ID = ?");


# DEL = DELETE
my $mainDEL = $dbh->prepare("DELETE FROM biomarker_main WHERE ENSG_ID = ?");

# IN = INSERT
# This statement handles both new insert and update. If there is a duplicate key,
# the whole row is updated:
my $mainIN = $dbh->prepare("INSERT INTO biomarker_main (ENSG_ID,
TYPE,
oldTYPE,
gene_name,
criterion,
source,
HPA,
rank_baseline,
rank_adipose_subc,
rank_adipose_visc,
rank_blood,
rank_liver,
rank_pancreatic,
rank_skelmus,
extracel_pred,
extracel_lit,
extracel_pred_lit,
extracel_man_cur,
mem_pred,
mem_lit,
mem_pred_lit,
lys_pot,
lys_blast,
signalP,
secretomeP,
in_plasma,
f_score) 
VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

#$mainIN->execute("222", "Mig", "Dig", "DM", "PHD", "1", '00000000');
#$mainUP->execute("Purple", "Lilla", "Purple", "worker", "0", "2014.05.13", "77522");
#$mainUP->execute("TheBeast", "Purple", "Lilla", "Purple", "worker", '00000000', "666");




open ("IN", "<", $filename) or die $!;
my $newheader = <IN>;

#my $len = 0;
#my $prelen = 0;
#my $count = 0;
while (defined(my $line = <IN>)) {
	chomp $line;
	my @el = split("\t", $line);
	#print "$el[$#el-3]\n";
	# Remove the antibody-info:
	my $rmindex = $#el-3;
	splice (@el, $rmindex, 3);
	#print "$el[$#el-3]\n";
	#$len = @el;
	#print join("*\n*",@el)."\n" if $len != $prelen;
	#print "current $len and previous $prelen count = $count\n" if $len != $prelen;
	#$prelen = $len;
	#$count++;
	# The information array is given twice: First if
	# new entry, second if duplicate key and update:
	if (exists $insert{$el[0]}) {
		#print "Now insert: $el[0]\n";
		$mainIN->execute(@el);
	} elsif (exists $update{$el[0]}) {
		$mainUP->execute(@el);
	} elsif (exists $there{$el[0]}) {
		$mainDEL->execute($el[0]);
	} else {
		die "Did I miss something??\n";
	}

}

close IN;




__END__