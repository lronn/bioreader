#!/usr/bin/perl

use strict;
use warnings;
use DBI;
use CGI::Lite;
use Time::Piece;


########################
### Setup CGI mdoule ###
########################

my $cgi = new CGI::Lite;
my %vars_head = $cgi->parse_form_data ('HEAD');
my %vars_post = $cgi->parse_form_data ('POST');

my $username = $ARGV[0];


############################
### Database connections ###
############################

sub db_connect {
	my $databaseName = 'krdav_private';
	my $dbuser = 'krdav';
	my $dbpassword = 'uUlZSoQm';

	# Make connection to server:
	my $dbc = DBI->connect("DBI:mysql:database=$databaseName;host=mysql.cbs.dtu.dk", $dbuser, $dbpassword, 
		{RaiseError => 1, AutoCommit => 1});
	return ($dbc);
}


my $dbh = db_connect ();





# Fetch votes for DB (ASC = last first so the old votes from same user will be overwritten by new ones):
my $dbh_event = $dbh->prepare ("SELECT * FROM biomarker_eventlog WHERE eventid NOT IN ( 1 ) ORDER BY datetime ASC") or die ("Could not prepare statement: " . $dbh->errstr);
$dbh_event->execute ();



my %protvote;
my %twoAB;
my %SC;
my %Yes;
my %No;
my %NOP;

while (my $event = $dbh_event->fetchrow_hashref ()) {
	my $evID = $event->{'eventid'};
	my $user = $event->{'user_name'};
	my $pid = $event->{'FK_autoID'};


	if ($evID == 2) {
		$protvote{"$pid:$user"} = 1;
		delete $twoAB{"$pid:$user"};

		if ($user eq "ABstatistics1" and !defined ${$SC{$pid}}[0]){
			${$SC{$pid}}[0] = "AB1";
		} elsif ($user eq "ABstatistics2" and !defined ${$SC{$pid}}[1]) {
			${$SC{$pid}}[1] = "AB2";
		}
	} elsif ($evID == 3) {
		$protvote{"$pid:$user"} = 1;
		$twoAB{"$pid:$user"} = 1;
	} elsif ($evID == 4) {
		$protvote{"$pid:$user"} = -1;
		delete $twoAB{"$pid:$user"};
	} elsif ($evID == 5) {
		$protvote{"$pid:$user"} = 0;
		delete $twoAB{"$pid:$user"};
	}
}




my %red;
my %yellow;
my %green;
my %twovote;

my %Yes;
my %No;
my %NOP;

my $curkey = 0;
foreach my $k (sort keys %protvote) {
	$curkey = (split(':', $k))[0];
	$twovote{$curkey} = 1 if $twoAB{$k};
	my $vote = $protvote{$k};

	if ($vote == 1) {
		$Yes{$curkey} = (exists $Yes{$curkey} ? $Yes{$curkey} + 1 : 1);
	} elsif ($vote == -1) {
		$No{$curkey} = (exists $No{$curkey} ? $No{$curkey} + 1 : 1);
	} elsif ($vote == 0) {
		$NOP{$curkey} = (exists $NOP{$curkey} ? $NOP{$curkey} + 1 : 1);
	}

	next if exists $yellow{$curkey};

	if ($vote == 1 and !exists $red{$curkey}) {
		$green{$curkey} = 1;
	} elsif ($vote == -1 and !exists $green{$curkey}) {
		$red{$curkey} = 1;
	} elsif ($vote == 0) {
		# No opinion does nothing
	} else {
		$yellow{$curkey} = 1;
		delete $green{$curkey} if exists $green{$curkey};
		delete $red{$curkey} if exists $red{$curkey};
	}
}



# Fetch antibodies:
my $dbh_anti = $dbh->prepare ("SELECT * FROM biomarker_antibodies") or die ("Could not prepare statement: " . $dbh->errstr);
$dbh_anti->execute ();

# ${$ABaval{$pid}}[0] = Green
# ${$ABaval{$pid}}[0] = Yellow
# ${$ABaval{$pid}}[0] = Red
my %ABaval;
while (my $anti = $dbh_anti->fetchrow_hashref ()) {
	my $pid = $anti->{'FK_autoID'};
	my $aval = ($anti->{'availability'} ? $anti->{'availability'} : 0);

	if ($aval == 3) {
		${$ABaval{$pid}}[0] = (defined ${$ABaval{$pid}}[0] ? ${$ABaval{$pid}}[0] + 1 : 1)	
	} elsif ($aval == 2) {
		${$ABaval{$pid}}[1] = (defined ${$ABaval{$pid}}[1] ? ${$ABaval{$pid}}[1] + 1 : 1)	
	} elsif ($aval == 1) {
		${$ABaval{$pid}}[2] = (defined ${$ABaval{$pid}}[2] ? ${$ABaval{$pid}}[2] + 1 : 1)	
	}
}



# Fetch absences data from DB: 
my $dbh_main = $dbh->prepare ("SELECT * FROM biomarker_main") or die ("Could not prepare statement: " . $dbh->errstr);
$dbh_main->execute ();


my $numbprot = $dbh_main->rows;

my %pidToName;
my %pidToENSG;
my $htmlLater;
while (my $main = $dbh_main->fetchrow_hashref ()) {
	my $pid = $main->{'auto'};
	my $ENSG = $main->{'ENSG_ID'};
	my $name = $main->{'gene_name'};
	my $SCvote = '0';
	my $ABA = 'na';
	my $Y = (exists $Yes{$pid} ? $Yes{$pid} : 0);
	my $N = (exists $No{$pid} ? $No{$pid} : 0);
	my $NOP = (exists $NOP{$pid} ? $NOP{$pid} : 0);
	$pidToName{$pid} = $name;
	$pidToENSG{$pid} = $ENSG;

	if (exists $ABaval{$pid} and defined ${$ABaval{$pid}}[0]) {
		$ABA = "${$ABaval{$pid}}[0]G";
		$ABA .= (defined ${$ABaval{$pid}}[1] ? ",${$ABaval{$pid}}[1]Y" : '');
		$ABA .= (defined ${$ABaval{$pid}}[2] ? ",${$ABaval{$pid}}[2]R" : '');

	} elsif (exists $ABaval{$pid} and defined ${$ABaval{$pid}}[1]) {
		$ABA = "${$ABaval{$pid}}[1]Y";
		$ABA .= (defined ${$ABaval{$pid}}[2] ? ",${$ABaval{$pid}}[2]R" : '');
	} elsif (exists $ABaval{$pid} and defined ${$ABaval{$pid}}[2]) {
		$ABA = "${$ABaval{$pid}}[2]R";
	}

	if (exists $SC{$pid} and defined ${$SC{$pid}}[0]) {
		#$SCvote = '1';
		$SCvote = join('_', @{$SC{$pid}});
	} elsif (exists $SC{$pid} and defined ${$SC{$pid}}[1]) {
		$SCvote = 'AB2';
	}

	my $rowcolor = '';
	my $status = 'Not defined';
	if (exists $green{$pid}) {
		$rowcolor = 'class="rowgreen"';
		$status = 'Green';
	} elsif (exists $red{$pid}) {
		$rowcolor = 'class="rowred"';
		$status = 'Red';
	} elsif (exists $yellow{$pid}) {
		$rowcolor = 'class="rowyellow"';
		$status = 'Yellow';
	}


	$htmlLater .= qq{
	        <tr $rowcolor>
	            <td><a href="ID.php?id=$ENSG" target="_blank">$ENSG</a></td>
	            <td>$status</td>
	            <td>$main->{'TYPE'}</td>
	            <td>$main->{'gene_name'}</td>
	            <td>$main->{'criterion'}</td>
	            <td>$ABA</td>
	            <td>$main->{'HPA'}</td>
	            <td>$main->{'f_score'}</td>
	            <td>$SCvote</td>
	            <td>$Y</td>
	            <td>$N</td>
	            <td>$NOP</td>
	            <td>$main->{'extracel_man_cur'}</td>
	            <td>$main->{'mem_pred_lit'}</td>	            
	            <td>$main->{'lys_pot'}</td>
	            <td>$main->{'lys_blast'}</td>
	            <td>$main->{'signalP'}</td>
	            <td>$main->{'secretomeP'}</td>
	            <td>$main->{'in_plasma'}</td>
	            <td>$main->{'rank_baseline'}</td>
	            <td>$main->{'rank_adipose_subc'}</td>
	            <td>$main->{'rank_adipose_visc'}</td>
	            <td>$main->{'rank_blood'}</td>
	            <td>$main->{'rank_liver'}</td>
	            <td>$main->{'rank_pancreatic'}</td>
	            <td>$main->{'rank_skelmus'}</td>
	        </tr>};

}


$htmlLater .= qq{
    </tbody>
</table>
};



#########################
## Main table columns ###
#########################

#ENSG_ID
#TYPE
#oldTYPE
#gene_name
#criterion
#source
#HPA
#rank_baseline
#rank_adipose_subc
#rank_adipose_visc
#rank_blood
#rank_liver
#rank_pancreatic
#rank_skelmus
#extracel_pred
#extracel_lit
#extracel_pred_lit
#extracel_man_cur
#mem_pred
#mem_lit
#mem_pred_lit
#lys_pot
#lys_blast
#signalP
#secretomeP
#in_plasma
#anti_id
#anti_volD<
#anti_conc
#f_score
my $greenENSG = '';
my $redENSG = '';
my $yellowENSG = '';
my $twovoteENSG = '';

my $greenName = '';
my $redName = '';
my $yellowName = '';
my $twovoteName = '';

foreach my $k (keys %pidToENSG) {
	if (exists $twovote{$k}) {
		$twovoteENSG .= "$pidToENSG{$k}; ";
		$twovoteName .= "$pidToName{$k}; ";
	}

	if (exists $green{$k}) {
		$greenENSG .= "$pidToENSG{$k}; ";
		$greenName .= "$pidToName{$k}; ";
		next;
	} elsif (exists $red{$k}) {
		$redENSG .= "$pidToENSG{$k}; ";
		$redName .= "$pidToName{$k}; ";
		next;
	} elsif (exists $yellow{$k}) {
		$yellowENSG .= "$pidToENSG{$k}; ";
		$yellowName .= "$pidToName{$k}; ";
		next;
	}
}

$greenENSG = substr($greenENSG, 0, -2);
$redENSG = substr($redENSG, 0, -2);
$yellowENSG = substr($yellowENSG, 0, -2);
$twovoteENSG = substr($twovoteENSG, 0, -2);

$greenName = substr($greenName, 0, -2);
$redName = substr($redName, 0, -2);
$yellowName = substr($yellowName, 0, -2);
$twovoteName = substr($twovoteName, 0, -2);

my $greens = scalar keys %green;
my $reds = scalar keys %red;
my $yellows = scalar keys %yellow;
my $twovotes = scalar keys %twoAB;


my $stats = qq{
			      	<div id="stats">
			      		<div class="centerdiv"><h4 class="statcenter" id="statgreen" data-content="This number represents proteins where a one or more PI's have fully agreed on rescuing." rel="popover" data-placement="bottom" data-original-title="Green" data-trigger="hover"><span>Green: $greens/$numbprot</span></h4><a class="linkcenter" href="#" data-toggle="modal" data-target="#greenstatModal"> Get the list<img src="link.gif"></a></div>
			      		<div class="centerdiv"><h4 class="statcenter" id="statred" data-content="This number represents proteins where a one or more PI's have fully agreed on abandoning." rel="popover" data-placement="bottom" data-original-title="Red" data-trigger="hover"><span>Red: $reds/$numbprot</span></h4><a class="linkcenter" href="#" data-toggle="modal" data-target="#redstatModal"> Get the list<img src="link.gif"></a></div>
			      		<div class="centerdiv"><h4 class="statcenter" id="statyellow" data-content="This number represents proteins where no unanimous decision have been made yet." rel="popover" data-placement="bottom" data-original-title="Yellow" data-trigger="hover"><span>Yellow: $yellows/$numbprot</span></h4><a class="linkcenter" href="#" data-toggle="modal" data-target="#yellowstatModal"> Get the list<img src="link.gif"></a></div>

			      		In addition the number of protein where two or more antibodies are voted for is: $twovotes <a href="#" data-toggle="modal" data-target="#twostatModal"> Get the list<img src="link.gif"></a>
};

$stats .= qq{
			      	</div> <!-- stats div -->
};





# Search bar:
my $html .= qq{
	<div class="row">
		<div class="col-md-5">
			<div class="dataTables_filter" id="movingSearch">
			    <label>
			    Search:
			    <input class="global_filter" id="global_filter" type="text">
				</label>
				<label>
			    <input class="global_filter" id="global_regex" type="checkbox"> Treat as list
			    </label>
		    </div>
		</div>
};
			    #<form action="Wlist.php" method="POST" target="_blank">
				#<input type="submit" id-"btn-form-submit" value="Walk through filtered list">
				#</form>


$html .= qq{
		<div class="col-md-7">
			<div id="runProtCount">
				<h1 style="display: inline">Proteins rescued: $greens/$numbprot</h1><a href="#" data-toggle="modal" data-target="#statModal"> More info<img src="link.gif"></a>
			</div>
		</div> <!-- col-md-7 -->
	</div> <!-- Row -->
};



#             <th rowspan="2"><p class="listheaderpopover" data-toggle="popover" data-placement="bottom" title="Votes" data-content="For vs. against. Non opinion does not count." data-trigger="hover">Votes</p></th>


$html .= qq{
<table id="marker_main" class="display">
    <thead>
        <tr>
            <th rowspan="2"><p>Ensembl ID</p></th>
            <th rowspan="2"><p class="listheaderpopover" data-toggle="popover" data-placement="bottom" title="Color code" data-content="Green = fully agreement of rescue. Yellow = split decision. Red = fully agreement of abandoning. Grey = no votes yet." data-trigger="hover">Status</p></th>
            <th rowspan="2"><p>Type</p></th>
            <th rowspan="2"><p>Gene name</p></th>
            <th rowspan="2"><p class="listheaderpopover" data-toggle="popover" data-placement="bottom" title="Criteria" data-content="PRna = Pancreas Rnaseq. Isex = Islet expression. Plap = Plasma profiling. PlapE = Plasma profiling for ENGAGE. Sclin = Suggested by clinician. Lmin = Literature mining. GeQTL = Gene_eQTL. Bcell = Beta-cell. Gmicro = Gut microbia. Prin = Proinsulin. GWASdie = GWAS-driven islet-expressed. Net = Network." data-trigger="hover">Criteria</p></th>
            <th rowspan="2"><p class="listheaderpopover" data-toggle="popover" data-placement="bottom" title="Antibody availability" data-content="Number and group of antibodies. E.g. 1G,2Y,3R means 1 antibody in the green grouping, two in the yellow and 3 in the red. Where green = available, yellow = limited availability, red = no availability." data-trigger="hover">ABA</p></th>
            <th rowspan="2"><p class="listheaderpopover" data-toggle="popover" data-placement="bottom" title="HPA count" data-content="The number of human protein antibodies directed at the selected protein. See list of antibodies on voting page for details of antibody availability." data-trigger="hover">HPA</p></th>
            <th rowspan="2"><p class="listheaderpopover" data-toggle="popover" data-placement="bottom" title="Feasibility score" data-content="Feasibility score based on the evaluation of protein secretion potential. The in silico screening of secreted proteins was based on an integrative effort comprising different predictors and literature annotations. Seven evidence sources (SignalP 4.1, TMHMM 2.0, TargetP 1.1, SecretomeP 1.0, Known lysosome proteins, PeptideAtlas, Literature: annotation from Uniprot) were combined in a final feasibility score ranging from 0-4, where 0 implies no evidence of secretion." data-trigger="hover">F-score</p></th>
            <th rowspan="2"><p class="listheaderpopover" data-toggle="popover" data-placement="bottom" title="Scoring criteria" data-content="Tells if the protein is in one of the two scoring criteria; ABstatistics1 or ABstatistics2. E.g. AB1_AB2 means that the protein is in both scoring criteria, while a 0 means that it is in non of them." data-trigger="hover">SC</p></th>
            <th rowspan="2"><p class="listheaderpopover" data-toggle="popover" data-placement="bottom" title="Yes votes" data-content="Number of yes votes including the ABstatistics votes." data-trigger="hover">Y</p></th>
            <th rowspan="2"><p class="listheaderpopover" data-toggle="popover" data-placement="bottom" title="No votes" data-content="Number of no votes." data-trigger="hover">N</p></th>
            <th rowspan="2"><p class="listheaderpopover" data-toggle="popover" data-placement="bottom" title="No opinion votes" data-content="Number of no opinion votes." data-trigger="hover">NOP</p></th>
            <th colspan="7"><p>Location</p></th>
            <th colspan="7"><p class="listheaderpopover" data-toggle="popover" data-placement="bottom" title="Meta rank" data-content="Integrative ranking of T2D association using MetaRanker 2.0 (Pers et al, 2013). MetaRanker integrates GWAS data, genetic regions from linkage studies, protein-protein interactions, disease similarity metrics from text mining, eQTL information from blood and tissue-specific gene expression to rank T2D candidate genes in a general and tissue-specific manner." data-trigger="hover">Meta rank</p></th>
        </tr>
        <tr>
        	<th><p>Extracellular</p></th>
        	<th><p>Membrane</p></th>
        	<th><p>Lysosomal potential</p></th>
        	<th><p>Lysosomal BLAST</p></th>
        	<th><p>SignalP</p></th>
        	<th><p>SecretomeP</p></th>
        	<th><p>In plasma</p></th>
        	<th><p>Baseline</p></th>
        	<th><p>Adipose subc.</p></th>
        	<th><p>Adipose visc.</p></th>
        	<th><p>Blood</p></th>
        	<th><p>Liver</p></th>
        	<th><p>Pancreatic</p></th>
        	<th><p>Skelmus</p></th>
        </tr>
    </thead>
    <tbody>};



### Insert htmlLater here:
$html .= $htmlLater;



# Modals:
$html .= qq{
<!-- Modal -->
<div class="modal fade" id="statModal" tabindex="-1" role="dialog" aria-labelledby="statModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="statModalLabel">Protein stats</h4>
      </div> <!-- modal header -->
      <div class="modal-body">
        $stats
      </div> <!-- modal body -->
      <div class="modal-footer">
      </div> <!--dummy modal footer -->
    </div> <!-- modal content -->
  </div> <!-- modal dialog -->
</div> <!-- modal dialog -->

<div class="modal fade" id="greenstatModal" tabindex="-1" role="dialog" aria-labelledby="greenstatModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="greenstatModalLabel">Proteins on the green list</h4>
      </div> <!-- modal header -->
      <div class="modal-body">
      	<h4>Gene name</h4>
        $greenName
      	<h4>ENSG ID</h4>
        $greenENSG
      </div> <!-- modal body -->
      <div class="modal-footer">
      </div> <!--dummy modal footer -->
    </div> <!-- modal content -->
  </div> <!-- modal dialog -->
</div> <!-- modal dialog -->

<div class="modal fade" id="redstatModal" tabindex="-1" role="dialog" aria-labelledby="redstatModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="redstatModalLabel">Proteins on the red list</h4>
      </div> <!-- modal header -->
      <div class="modal-body">
      	<h4>Gene name</h4>
	$redName
	<h4>ENSG ID</h4>
        $redENSG
      </div> <!-- modal body -->
      <div class="modal-footer">
      </div> <!--dummy modal footer -->
    </div> <!-- modal content -->
  </div> <!-- modal dialog -->
</div> <!-- modal dialog -->

<div class="modal fade" id="yellowstatModal" tabindex="-1" role="dialog" aria-labelledby="yellowstatModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="yellowstatModalLabel">Proteins on the yellow list</h4>
      </div> <!-- modal header -->
      <div class="modal-body">
      	<h4>Gene name</h4>
	$yellowName
	<h4>ENSG ID</h4>
        $yellowENSG
      </div> <!-- modal body -->
      <div class="modal-footer">
      </div> <!--dummy modal footer -->
    </div> <!-- modal content -->
  </div> <!-- modal dialog -->
</div> <!-- modal dialog -->

<div class="modal fade" id="twostatModal" tabindex="-1" role="dialog" aria-labelledby="twostatModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="twostatModalLabel">Proteins voted for two or more antibodies</h4>
      </div> <!-- modal header -->
      <div class="modal-body">
      	<h4>Gene name</h4>
	$twovoteName
	<h4>ENSG ID</h4>
        $twovoteENSG
      </div> <!-- modal body -->
      <div class="modal-footer">
      </div> <!--dummy modal footer -->
    </div> <!-- modal content -->
  </div> <!-- modal dialog -->
</div> <!-- modal dialog -->
};



print "$html\n";
__END__












