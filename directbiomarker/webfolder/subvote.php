<?php

/*
/	This array is going to be populated with either
/	the data that was sent to the script, or the
/	error messages:
/*/

$servername = "mysql.cbs.dtu.dk";
$username = "krdav";
$password = "uUlZSoQm";
$dbname = "krdav_private";

/*
Put in some data to $_POST to simulate a comment

var_dump($_POST);
echo $_POST["FK_autoID"];
echo "More text";
exit();
$_POST["FK_autoID"] = '417';
$_POST["user_name"] = 'krdav';
$_POST["pcomment"] = 'This is a test';
*/

# Username after login:
$login = getenv('REMOTE_USER');
#print "$login\n";



$arr = array(
	'FK_autoID' => $_POST["FK_autoID"],
	'user_name' => $login,
	'eventid' => $_POST["RadioProt"],
	//'comment' => $_POST["pcomment"]
);

$date = new DateTime('Europe/Berlin');
$dt = $date->format('Y-m-d H:i:s');
//print "$dt\n";
$arr['datetime'] = $dt;

if ($arr['eventid'] == 2 and $_POST["ABcheck"]) {
	$arr['eventid'] = 3;
}

$validates = 1;

if ($validates) {
    /* Everything is OK, insert to database: */

	/*
    */
    $mysqli = new mysqli($servername, $username, $password, $dbname);

    if (mysqli_connect_errno()) {
    	printf("Connect failed: %s\n", mysqli_connect_error());
    	exit();
	}
	$mysqli->query("	INSERT INTO biomarker_eventlog(FK_autoID, user_name, eventid, datetime)
                    VALUES (
                        '".$arr['FK_autoID']."',
                        '".$arr['user_name']."',
                        '".$arr['eventid']."',
                        '".$arr['datetime']."'
                    )");

    $evID = $mysqli->insert_id;

    $mysqli->close();

    if ($arr['eventid'] == 2) {
    	print "<p id=\"eventid".$evID."\">".$arr['datetime']." - <b>".$arr['user_name']." included this protein with one antibody</b>\n<img src=\"delete.png\" height=\"15\" width=\"15\" class=\"deleteEventPic\" name=\"".$evID."\"></p>";
    } elseif ($arr['eventid'] == 3) {
    	print "<p id=\"eventid".$evID."\">".$arr['datetime']." - <b>".$arr['user_name']." included this protein with two or more antibodies</b>\n<img src=\"delete.png\" height=\"15\" width=\"15\" class=\"deleteEventPic\" name=\"".$evID."\"></p>";
    } elseif ($arr['eventid'] == 4) {
    	print "<p id=\"eventid".$evID."\">".$arr['datetime']." - <b>".$arr['user_name']." excluded this protein</b>\n<img src=\"delete.png\" height=\"15\" width=\"15\" class=\"deleteEventPic\" name=\"".$evID."\"></p>";
    } elseif ($arr['eventid'] == 5) {
    	print "<p id=\"eventid".$evID."\">".$arr['datetime']." - <b>".$arr['user_name']." had no opinion on this protein</b>\n<img src=\"delete.png\" height=\"15\" width=\"15\" class=\"deleteEventPic\" name=\"".$evID."\"></p>";
    }


    //print $arr['antibody'];

} else {
    print "Fail";
}


?>