#!/usr/bin/perl

use strict;
use warnings;
use DBI;
use CGI::Lite;
use Time::Piece;

my $ENSG_ID = $ARGV[0];

my $username = $ARGV[1];
#my $username = 'krdav';

# Sanity check on the ENSG_ID:
unless ($ENSG_ID =~ /^ENSG\d+$/ or $ENSG_ID =~ /^NoID\d+$/) {
	print "$ENSG_ID is not a valid ID\n";
	die;
}

# Print ENSG_ID:
#my $html .= '<p>'. $ENSG_ID . '</p>';
#print "$html\n";

#__END__

############################
### Database connections ###
############################

sub db_connect {
	my $databaseName = 'krdav_private';
	my $dbuser = 'krdav';
	my $dbpassword = 'uUlZSoQm';

	# Make connection to server:
	my $dbc = DBI->connect("DBI:mysql:database=$databaseName;host=mysql.cbs.dtu.dk", $dbuser, $dbpassword, 
		{RaiseError => 1, AutoCommit => 1});
	return ($dbc);
}


my $dbh = db_connect ();



# Check if the ID is in the database: 
my $dbh_onlist = $dbh->prepare (qq(SELECT EXISTS(SELECT 1 FROM biomarker_main WHERE ENSG_ID = "$ENSG_ID") AS onlist)) or die ("Could not prepare statement: " . $dbh->errstr);
$dbh_onlist->execute ();

my $onlist = $dbh_onlist->fetchrow_hashref ();

unless ($onlist->{'onlist'}) {
	print "No instance of $ENSG_ID in database\n";
	die;
}



# Now fetch data:
my $dbh_prot = $dbh->prepare (qq(SELECT * FROM biomarker_main WHERE ENSG_ID = "$ENSG_ID")) or die ("Could not prepare statement: " . $dbh->errstr);
$dbh_prot->execute ();

my $prot = $dbh_prot->fetchrow_hashref ();

# The primary key in main table:
my $protid = $prot->{'auto'};


# Fetch from the eventlog:
my $dbh_event = $dbh->prepare (qq(SELECT * FROM biomarker_eventlog WHERE FK_autoID = "$protid" ORDER BY datetime DESC)) or die ("Could not prepare statement: " . $dbh->errstr);
$dbh_event->execute ();


# Fetch antibodies:
my $dbh_anti = $dbh->prepare (qq(SELECT * FROM biomarker_antibodies WHERE FK_autoID = "$protid")) or die ("Could not prepare statement: " . $dbh->errstr);
$dbh_anti->execute ();



my $longtext = "
Lorem Ipsum is simply dummy text of the printing and typesetting industry.\n
Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown\n
printer took a galley of type and scrambled it to make a type specimen book. It has survived not\n
only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was \n
popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,\n
and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n
Lorem Ipsum is simply dummy text of the printing and typesetting industry.\n
Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown\n
printer took a galley of type and scrambled it to make a type specimen book. It has survived not\n
only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was \n
popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,\n
and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n
Lorem Ipsum is simply dummy text of the printing and typesetting industry.\n
Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown\n
printer took a galley of type and scrambled it to make a type specimen book. It has survived not\n
only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was \n
popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,\n
and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n
Lorem Ipsum is simply dummy text of the printing and typesetting industry.\n
Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown\n
printer took a galley of type and scrambled it to make a type specimen book. It has survived not\n
only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was \n
popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,\n
and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n
";




my $html;
=pod;
# Basic info and Meta rank table:
my $html .= qq{
<div id="basic_rank">
	<table id="table_basic_rank">
	<tr class="table_info_head">
		<th colspan="2">Basic info</th>
	<tr>
		<th><p>Ensembl ID</p></th>
		<th><p>$ENSG_ID</p></th>
	</tr>
	<tr>
		<th><p>Type</p></th>
		<th><p>$prot->{'TYPE'}</p></th>
	</tr>
	<tr>
		<th><p>Old type</p></th>
		<th><p>$prot->{'oldTYPE'}</p></th>
	</tr>
	<tr>
		<th><p>Source</p></th>
		<th><p>$prot->{'source'}</p></th>
	</tr>
	<tr>
		<th><p>Gene name</p></th>
		<th><p>$prot->{'gene_name'}</p></th>
	</tr>
	<tr>
		<th><p>Criteria</p></th>
		<th><p>$prot->{'criterion'}</p></th>
	</tr>
	<tr>
		<th><p class="listheaderpopover" data-toggle="popover" data-placement="bottom" title="Feasibility score" data-content="Feasibility score based on the evaluation of protein secretion potential. The in silico screening of secreted proteins was based on an integrative effort comprising different predictors and literature annotations. Seven evidence sources (SignalP 4.1, TMHMM 2.0, TargetP 1.1, SecretomeP 1.0, Known lysosome proteins, PeptideAtlas, Literature: annotation from Uniprot) were combined in a final feasibility score ranging from 0-4, where 0 implies no evidence of secretion." data-trigger="hover">Feasibility score</p></th>
	</tr>
	<tr>
		<th><p>Some other score</p></th>
		<th><p>That will replace novelty score</p></th>
	</tr>
	<tr class="table_info_head">
		<th colspan="2"><p class="listheaderpopover" data-toggle="popover" data-placement="bottom" title="Meta rank" data-content="Integrative ranking of T2D association using MetaRanker 2.0 (Pers et al, 2013). MetaRanker integrates GWAS data, genetic regions from linkage studies, protein-protein interactions, disease similarity metrics from text mining, eQTL information from blood and tissue-specific gene expression to rank T2D candidate genes in a general and tissue-specific manner." data-trigger="hover">Meta rank</p></th>
	<tr>
	<tr>
		<th><p>Baseline</p></th>
		<th><p>$prot->{'rank_baseline'}</p></th>
	</tr>
	<tr>
		<th><p>Adipose subcutaneous</p></th>
		<th><p>$prot->{'rank_adipose_subc'}</p></th>
	</tr>
	<tr>
		<th><p>Blood</p></th>
		<th><p>$prot->{'rank_blood'}</p></th>
	</tr>
	<tr>
		<th><p>Liver</p></th>
		<th><p>$prot->{'rank_liver'}</p></th>
	</tr>
	<tr>
		<th><p>Pancreas</p></th>
		<th><p>$prot->{'rank_pancreatic'}</p></th>
	</tr>
	<tr>
		<th><p>Skelmus</p></th>
		<th><p>$prot->{'rank_skelmus'}</p></th>
	</tr>
	</table>
</div>};



# Location table:
$html .= qq{
<div id="location">
	<table id="table_location">
	<tr class="table_info_head">
		<th colspan="2">Location in the cell</th>
	<tr>
	<tr>
		<th><p>Extracellular predicion</p></th>
		<th><p>$prot->{'extracel_pred'}</p></th>
	</tr>
	<tr>
		<th><p>Extracellular litterature</p></th>
		<th><p>$prot->{'extracel_lit'}</p></th>
	</tr>
	<tr>
		<th><p>Extracellular combined score</p></th>
		<th><p>$prot->{'extracel_pred_lit'}</p></th>
	</tr>
	<tr>
		<th><p>Extracellular manual curation</p></th>
		<th><p>$prot->{'extracel_man_cur'}</p></th>
	</tr>
	<tr>
		<th><p>Membrane predicion</p></th>
		<th><p>$prot->{'mem_pred'}</p></th>
	</tr>
	<tr>
		<th><p>Membrane litterature</p></th>
		<th><p>$prot->{'mem_lit'}</p></th>
	</tr>
	<tr>
		<th><p>Membrane combined score</p></th>
		<th><p>$prot->{'mem_pred_lit'}</p></th>
	</tr>
	<tr>
		<th><p>Lysosomal potential</p></th>
		<th><p>$prot->{'lys_pot'}</p></th>
	</tr>
	<tr>
		<th><p>Lysosomal BLAST</p></th>
		<th><p>$prot->{'lys_blast'}</p></th>
	</tr>
	<tr>
		<th><p>SignalP predicion</p></th>
		<th><p>$prot->{'signalP'}</p></th>
	</tr>
	<tr>
		<th><p>SecretomeP predicion</p></th>
		<th><p>$prot->{'secretomeP'}</p></th>
	</tr>
	<tr>
		<th><p>In plasma</p></th>
		<th><p>$prot->{'in_plasma'}</p></th>
	</tr>
	<tr>
		<th><p>Membrane combined score</p></th>
		<th><p>$prot->{'mem_pred_lit'}</p></th>
	</tr>
	<tr>
		<th><p>Membrane combined score</p></th>
		<th><p>$prot->{'mem_pred_lit'}</p></th>
	</tr>
	</table>
</div>
};




$html .= qq{
<div id="voting">

</div>
};


$html .= qq{
<div id="log">

</div>
};


$html .= qq{
<div id="proteom">

</div>
};
=cut;
#http://www.ensembl.org/Homo_sapiens/Gene/Summary?g=



my $name_type = qq{
										<table id="tname_type">
										<tr>
											<th class="b_left">Ensembl ID</th>
											<th class="b_right"><a href="http://www.ensembl.org/Homo_sapiens/Gene/Summary?g=$prot->{'ENSG_ID'}" target="_blank">$prot->{'ENSG_ID'}</a></th>
										</tr>
										<tr>
											<th class="b_left">Type</th>
											<th class="b_right">$prot->{'TYPE'}</th>
										</tr>
										<tr>
											<th class="b_left">Old type</th>
											<th class="b_right">$prot->{'oldTYPE'}</th>
										</tr>
										<tr>
											<th class="b_left">Source</th>
											<th class="b_right">$prot->{'source'}</th>
										</tr>
										<tr>
											<th class="b_left">Gene name</th>
											<th class="b_right">$prot->{'gene_name'}</th>
										</tr>
										<tr>
											<th class="b_left">Criteria</th>
											<th class="b_right">$prot->{'criterion'}</th>
										</tr>
										<tr>
											<th class="b_left">Feasibility score</th>
											<th class="b_right">$prot->{'f_score'}</th>
										</tr>
										</table>
};



my $meta_rank = qq{
										<table id="tmeta_rank">
										<tr>
											<th class="b_left">Baseline</th>
											<th class="b_right">$prot->{'rank_baseline'}</th>
										</tr>
										<tr>
											<th class="b_left">Adipose subcutaneous</th>
											<th class="b_right">$prot->{'rank_adipose_subc'}</th>
										</tr>
										<tr>
											<th class="b_left">Blood</th>
											<th class="b_right">$prot->{'rank_blood'}</th>
										</tr>
										<tr>
											<th class="b_left">Liver</th>
											<th class="b_right">$prot->{'rank_liver'}</th>
										</tr>
										<tr>
											<th class="b_left">Pancreas</th>
											<th class="b_right">$prot->{'rank_pancreatic'}</th>
										</tr>
										<tr>
											<th class="b_left">Skelmus</th>
											<th class="b_right">$prot->{'rank_skelmus'}</th>
										</tr>
										</table>
};



my $location = qq{
										<table id="tlocation">
										<tr>
											<th class="b_left">Extracellular prediction</th>
											<th class="b_right">$prot->{'extracel_pred'}</th>
										</tr>
										<tr>
											<th class="b_left">Extracellular litterature</th>
											<th class="b_right">$prot->{'extracel_lit'}</th>
										</tr>
										<tr>
											<th class="b_left">Extracellular combined score</th>
											<th class="b_right">$prot->{'extracel_pred_lit'}</th>
										</tr>
										<tr>
											<th class="b_left">Extracellular manual curation</th>
											<th class="b_right">$prot->{'extracel_man_cur'}</th>
										</tr>
										<tr>
											<th class="b_left">Membrane prediction</th>
											<th class="b_right">$prot->{'mem_pred'}</th>
										</tr>
										<tr>
											<th class="b_left">Membrane litterature</th>
											<th class="b_right">$prot->{'mem_lit'}</th>
										</tr>
										<tr>
											<th class="b_left">Membrane combined score</th>
											<th class="b_right">$prot->{'mem_pred_lit'}</th>
										</tr>
										<tr>
											<th class="b_left">Lysosomal potential</th>
											<th class="b_right">$prot->{'lys_pot'}</th>
										</tr>
										<tr>
											<th class="b_left">Lysosomal BLAST</th>
											<th class="b_right">$prot->{'lys_blast'}</th>
										</tr>
										<tr>
											<th class="b_left">SignalP prediction</th>
											<th class="b_right">$prot->{'signalP'}</th>
										</tr>
										<tr>
											<th class="b_left">SecretomeP prediction</th>
											<th class="b_right">$prot->{'secretomeP'}</th>
										</tr>
										<tr>
											<th class="b_left">In plasma</th>
											<th class="b_right">$prot->{'in_plasma'}</th>
										</tr>
										</table>
};




my $events = qq{
				                		<div>
};


# Delete event:
# <img src="delete.png" height="15" width="15">


#1: Comment
#2: Vote protein yes with one antibody
#3: Vote protein yes with two or more antibodies
#4: Vote protein no
#5: Vote protein no opinion
#6: Edit
while (my $event = $dbh_event->fetchrow_hashref ()) {
	# Only year-month-day:
	#my $date = substr($event->{'datetime'}, 0, 10);
	# Full datetime:
	my $date = $event->{'datetime'};
	my $autoID = $event->{'auto'};
	if ($event->{'eventid'} == 1) {
		$events .= qq{
											<p id="eventid$autoID">$date - <b>Comment from $event->{'user_name'}:</b> $event->{'comment'}&nbsp;<b>
		};
	} elsif ($event->{'eventid'} == 2) {
		$events .= qq{
											<p id="eventid$autoID">$date - <b>$event->{'user_name'} included this protein with one antibody
		};
	} elsif ($event->{'eventid'} == 3) {
		$events .= qq{
											<p id="eventid$autoID">$date - <b>$event->{'user_name'} included this protein with two or more antibodies
		};
	}elsif ($event->{'eventid'} == 4) {
		$events .= qq{
											<p id="eventid$autoID">$date - <b>$event->{'user_name'} excluded this protein
		};
	} elsif ($event->{'eventid'} == 5) {
		$events .= qq{
											<p id="eventid$autoID">$date - <b>$event->{'user_name'} had no opinion on this protein
		};
	}

	if ($event->{'user_name'} eq $username) {
		$events .= qq{</b><img src="delete.png" height="15" width="15" class="deleteEventPic" name="$autoID"></p>};
	} else {
		$events .= qq{</b></p>};
	}
}

$events .= qq{
				                		</div>
};




#### Make the checkboxes to vote for antibodies:
my $ABlist = qq{
				 						<div id="ABlist">               		
};

my $ABcol1 = qq{
										<div class="ABcolumn">
											<ul class="ABul">
};

my $ABcol2 = qq{
										<div class="ABcolumn">
											<ul class="ABul">
};



my $i = 1;
while (my $antibody = $dbh_anti->fetchrow_hashref ()) {
	my $id = $antibody->{'antiID'};
	my $class = 'ABlistEl';

	if ($antibody->{'availability'} == 1) {
		$class = 'ABlistElred'
	} elsif ($antibody->{'availability'} == 2) {
		$class = 'ABlistElyellow'
	} elsif ($antibody->{'availability'} == 3) {
		$class = 'ABlistElgreen'
	}

	# Put into new column at every second antibody:
	if (($i % 2) == 0) {
		$ABcol2 .= qq{
												<li class="$class">$id</li>
	};

	} else {
		$ABcol1 .= qq{
												<li class="$class">$id</li>
	};

	}

	$i++;
}


$ABcol1 .= qq{
											</ul>
										</div>
};

$ABcol2 .= qq{
											</ul>
										</div>
};

$ABlist .= $ABcol1 . $ABcol2; 





$ABlist .= qq{
				                		</div><!--ABlist-->
};



$html .= qq{

	<div class="row">
			<div class="col-md-7">
				<fieldset>
				    <legend>Voting poll</legend>
				    <div id="l1-well" class="well">
				    	<div id="voting_well" class="'pre-scrollable">
				            <div class="span6">
				                <div class="row">
									<form role="form" action="" id="makeVoteForm">
										<div class="col-md-3">
					                    <label class="control-label" for="protein_or_antibody">Protein vote:</label>
					                    <div id="protradios">
						                    <div class="radio">
											  <label>
											    <input type="radio" name="RadioProt" id="includeProt" value="2" class="ProtRadio">
											    Include
											  </label>
											</div>
											<div class="radio">
											  <label>
											    <input type="radio" name="RadioProt" id="excludeProt" value="4" class="ProtRadio">
											    Exclude
											  </label>
											</div>
											<div class="radio">
											  <label>
											    <input type="radio" name="RadioProt" id="opnonProt" value="5" class="ProtRadio" checked>
											    No opinion
											  </label>
											</div>
										</div>

  										 
										</div><!--Protein vote-->

										<div class="col-md-3">
										<label class="control-label" for="protein_or_antibody">Antibody vote:</label><br>
											<div class="checkbox" id="moreAB">
												<label>
										    	<input type="checkbox" name="ABcheck" id="moreABin" value="3" class="ABcheck">
										    	Two or more
										  	</label>
										  	</div>
										</div><!--Antibody vote-->

										<div class="col-md-4">
										<label class="control-label" for="protein_or_antibody"><p class="labelpopover" data-toggle="popover" data-placement="bottom" title="List of antibodies" data-content="Background color defines antibody availability: green = available, yellow = limited availability, red = no availability. No background color means no information." data-trigger="hover">List of antibodies</p></label><br>
											$ABlist
										</div><!--Antibody list-->
										
										<div class="col-md-2">
										  <input type="hidden" name="FK_autoID" value="$protid"/>
										  <button type="submit" class="btn btn-primary" id="votesub">Submit</button>
										</div><!--Submit vote-->
									</form>
								</div><!--row-->																	
				            </div><!--span-->
				        </div><!--voting_well-->
				    </div><!--well-->
				</fieldset>
				<fieldset>
				    <legend>Basic information</legend>
				    <div id="l1-well" class="well">
				        <div id="basic_well" class="'pre-scrollable">
				            <div class="span6">
				                <div class="row">
									<div class="col-md-6">
				                				<h3>Name and type information</h3>
										$name_type
										<br>
										<h3 class="listheaderpopover" style="width:22%" data-toggle="popover" data-placement="right" title="Meta rank" data-content="Integrative ranking of T2D association using MetaRanker 2.0 (Pers et al, 2013). MetaRanker integrates GWAS data, genetic regions from linkage studies, protein-protein interactions, disease similarity metrics from text mining, eQTL information from blood and tissue-specific gene expression to rank T2D candidate genes in a general and tissue-specific manner." data-trigger="hover">Meta rank</h3>
										$meta_rank
									</div>
										<div class="col-md-6">
				                		<h3>Location</h3>
				                		$location
									</div>
								</div>
				            </div><!--span-->
				        </div><!--basic_well-->
				    </div><!--well-->
				</fieldset>
			</div>
			<div class="col-md-5">
				<fieldset>
				    <legend>Events</legend>
				    <div id="l1-well" class="well">
				        <div id="event_well" class="'pre-scrollable">
				            <div class="span6">
								<form role="form" id="addCommentForm" action="submit.php" method="post">
  									<div class="form-group">
						    			<label for="commentbox">Comment this protein:</label>
						    			<textarea name="pcomment" class="form-control" rows="3" placeholder="Add comment" required></textarea>
				            		</div>
				            		<div class="btn-group btn-group-sm">
				            			<input type="hidden" name="FK_autoID" value="$protid"/>
				            			<p>
				            			<button type="submit" class="btn btn-primary" id="subcom">Submit</button>
				            			</p>
				            		</div>
				            	</form>
				                	<label class="control-label" for="events">Events:</label>
				            		<div id="eventdiv">
				                		$events
									</div>

				            </div><!--span-->


				        </div><!--event_well-->
				    </div><!--well-->
				</fieldset>
			</div>
	</div>
};


=pod;
$html .= qq{
	<div class="row">
		<div class="col-md-12">
			<fieldset>
			    <legend>Proteome data</legend>
			    <div id="l1-well" class="well">
			        <div class="control-group">
			            <div class="span6">
			                <label class="control-label" for="proteom">Here comes proteome data</label>
			                <div class="row">
								<div class="col-md-6">
									<p>Blaa blaa blaa</p>
								</div>
							</div>
			            </div><!--span-->
			        </div><!--control-group-->
			    </div><!--well-->
			</fieldset>
		</div>
	</div>
};
=cut;



print "$html\n";


__END__
