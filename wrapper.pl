#!/tools/bin/perl
use strict;
use Bio::DB::EUtilities;
use Getopt::Long;
use Cwd;


my $job_dir = '';
my $email = '';
my $ft = '';
my $verbose = "f";
my $set1 = '';
my $set2 = '';
my $test = '';
my $warns = '';

GetOptions("set1=s"=>\$set1, "set2=s"=>\$set2, "test=s"=>\$test, "d=s"=>\$job_dir, "e=s"=>\$email, "t=s"=>\$ft, "v=s"=>\$verbose);
# print "*$set1* *$set2* *$test* *$job_dir* *$email* *$ft* *$verbose*\n";



# my $email = 'lro@cbs.dtu.dk'; # User emails are used now
my $job_dir="/usr/opt/www/pub/CBS/services/BioReader-1.2/tmp/bioreader-".$$."-".time; # A good unique name
mkdir("$job_dir") or die "cannt make folder: $job_dir";

$email = 'mr.nobody@HideAndSeek.org';
if ($email eq "") {
	die "you must enter email address\n";
}

mkdir("$job_dir/training_corpus");
mkdir("$job_dir/test_corpus");



# print "*$set1* *$set2* *$test* *$job_dir* $email* *$ft* *$verbose*\n";


# Read the input PubMed IDs
my $i=1; #counter for 1,2,3 of the class sets
my @files;
if (defined($test) and $test ne '') {
    @files = ("$set1", "$set2", "$test");
} else {
    @files = ("$set1", "$set2");
}
#print "@files\n";

if ($verbose eq "t") {print "<h4>Actions:</h4>\n"};

# Check if both "set_test" and a search term is filled:
if ($test ne '' and $ft ne '') {
    print "<p>Results from your search term will be merged with the PMID you pasted into the text box.</p>\n" if $verbose eq "t";
}



#Open output file (index.txt)
open (my $fh_out, ">","$job_dir/index.txt") or die "Unable to open file: '$job_dir/index.txt'  $!";
foreach my $file (@files) {
        # print "<br>$ARGV\n<br>";
        my $fh_input;
        open($fh_input, '<', $file) or die "Unable to read input file: $file $!";
        my @raw_input = <$fh_input>;
        close $fh_input;
        my $input="";
        foreach my $idx (@raw_input) {
                $idx =~ s/\n/ /g;
                $input.=$idx;
        }
        my @pmid = split(/\s+/,$input);
        # print "@pmid - ",scalar @pmid,"<br>\n";
        foreach my $pmid (@pmid) {
                print $fh_out "$pmid\t$i\n";
        }
        $i++;
}
close $fh_out;






if ($verbose eq "t" and $ft ne '') {print "<p>Fetching query<p>\n"};


my $term;
my @pmids;
my @queryids;
my %index;
my $max = 1000 - scalar @pmids;
if ($ft ne '') {
        open(IN, '<', $ft) or die "Unable to read input file: $ft $!";
        chomp($term = <IN>);
	close IN;
        my $factory = Bio::DB::EUtilities->new(-eutil => 'esearch', -db => 'pubmed', -term => $term, -email => $email, -retmax => 1000);
	@queryids = $factory->get_ids;
	if ($term ne "") {
            if (scalar @queryids >= 1000) { # Max return from search function
                print "<p>", scalar @queryids, " or more abstracts were found for search term \'", $factory->get_query_translation, "\' </p>\n" if $verbose eq "t";
                $warns .= "<p>", scalar @queryids, " or more abstracts were found for search term \'", $factory->get_query_translation, "\' </p>\n";
            } else {
                 print "<p>", scalar @queryids, " abstracts were found for search term \'", $factory->get_query_translation, "\' </p>\n" if $verbose eq "t";
            }
        }
        #foreach my $id (@queryids) {
	#	$index{$id} = 3;
	#}
}
#print "*********$term**********\n";
# Warn if more than 1000 results, only keep 1000:
#if (scalar @queryids > 1000) {
#    if (scalar @queryids == 10000) { # Max return from search function
#        print "<p><b>Warning!</b> PubMed search returned more than " . scalar @queryids ." results. Only the top 1000 was kept.</p>" if $verbose eq "t";
#        $warns .= "<p><b>Warning!</b> PubMed search returned more than " . scalar @queryids ." results. Only the top 1000 was kept.</p>";
#    } else {
#        print "<p><b>Warning!</b> PubMed search returned " . scalar @queryids ." results. Only the top 1000 was kept.</p>" if $verbose eq "t";
#        $warns .= "<p><b>Warning!</b> PubMed search returned " . scalar @queryids ." results. Only the top 1000 was kept.</p>" if $verbose eq "t";
#    }
#    @queryids = splice(@queryids, 0, 1000);
#}


open IN, "<", "$job_dir/index.txt" or die $!;
my $line = <IN>;
while (defined $line) {
	chomp $line;
	$line =~ m/(\d*)\t(\d)/;
	$index{$1} = $2;
	push @pmids, $1;
	# print "$index{$1}\n";
	$line = <IN>;
}
close IN;
push(@pmids, @queryids);

if ($verbose eq "t") {print "<p>Fetching pubmed abstracts...</p>\n"};


#############
#my $factory = Bio::DB::EUtilities->new(-eutil => 'efetch', -db => 'pubmed', -rettype => 'gb', -email => $email, -id => \@pmids);
#my $file = "$job_dir/abstracts.xml";
#$factory->get_Response(-file => $file);

########## Three batch downloads:
# Get the PMIDS:
my @pmids_set1 = grep { $index{$_} eq 1 } keys %index;
my @pmids_set2 = grep { $index{$_} eq 2 } keys %index;
my @pmids_test = grep { $index{$_} eq 3 } keys %index;

# Are there too many inputs:
if (scalar @pmids_set1 > 1000) {
    print "<p><b>Warning!</b> Positive category contains " . scalar @pmids_set1 ." PMIDS. Only the first 1000 was kept.</p>" if $verbose eq "t";
    @pmids_set1 = splice(@pmids_set1, 0, 1000);
    print "@pmids_set1\n" if $verbose eq "t";
    $warns .= "<p><b>Warning!</b> Positive category contains " . scalar @pmids_set1 ." PMIDS. Only the first 1000 was kept.</p>";
}

if (scalar @pmids_set2 > 1000) {
    print "<p><b>Warning!</b> Negative category contains " . scalar @pmids_set2 ." PMIDS. Only the first was 1000 kept.</p>" if $verbose eq "t";
    @pmids_set2 = splice(@pmids_set2, 0, 1000);
    $warns .= "<p><b>Warning!</b> Negative category contains " . scalar @pmids_set2 ." PMIDS. Only the first was 1000 kept.</p>";
}

if (scalar @pmids_test > 1000) {
    print "<p><b>Warning!</b> Documents to test contains " . scalar @pmids_test ." PMIDS. Only the first was 1000 kept.</p>" if $verbose eq "t";
    @pmids_test = splice(@pmids_test, 0, 1000);
    $warns .= "<p><b>Warning!</b> Documents to test contains " . scalar @pmids_test ." PMIDS. Only the first was 1000 kept.</p>";
}


#### Fetch set1:
my $sets = int(scalar @pmids_set1 / 100);
my $i = 0;
while (@pmids_set1) {
    my @pmids_set1_slice = splice @pmids_set1, 0, 100;
    my $factory = Bio::DB::EUtilities->new(-eutil => 'efetch', -db => 'pubmed', -rettype => 'gb', -email => $email, -id => \@pmids_set1_slice);
    my $file = "$job_dir/abstracts_set1_$i.xml";
    $factory->get_Response(-file => $file);
    $i++;
}
system("cat $job_dir/abstracts_set1_*.xml > $job_dir/abstracts_set1.xml");

### Fetch set2:
my $sets = int(scalar @pmids_set2 / 100);
my $i = 0;
while (@pmids_set2) {
    my @pmids_set2_slice = splice @pmids_set2, 0, 100;
    my $factory = Bio::DB::EUtilities->new(-eutil => 'efetch', -db => 'pubmed', -rettype => 'gb', -email => $email, -id => \@pmids_set2_slice);
    my $file = "$job_dir/abstracts_set2_$i.xml";
    $factory->get_Response(-file => $file);
    $i++;
}
system("cat $job_dir/abstracts_set2_*.xml > $job_dir/abstracts_set2.xml");

### Fetch test set:
my $sets = int(scalar @pmids_test / 100);
my $i = 0;
while (@pmids_test) {
    my @pmids_test_slice = splice @pmids_test, 0, 100;
    my $factory = Bio::DB::EUtilities->new(-eutil => 'efetch', -db => 'pubmed', -rettype => 'gb', -email => $email, -id => \@pmids_test_slice);
    my $file = "$job_dir/abstracts_test_$i.xml";
    $factory->get_Response(-file => $file);
    $i++;
}
system("cat $job_dir/abstracts_test_*.xml > $job_dir/abstracts_test.xml");


# Now add the query to the test set:
foreach my $id (@queryids) {
    $index{$id} = 3;
}

### Fetch test set from query:
if (scalar @queryids >= 1) {
    my $sets = int(scalar @queryids / 100);
     my $i = 0;
     while (@queryids) {
         my @queryids_slice = splice @queryids, 0, 100;
         my $factory = Bio::DB::EUtilities->new(-eutil => 'efetch', -db => 'pubmed', -rettype => 'gb', -email => $email, -id => \@queryids_slice);
         my $file = "$job_dir/abstracts_testquery_$i.xml";
         $factory->get_Response(-file => $file);
         $i++;
    }
}
system("cat $job_dir/abstracts_testquery_*.xml > $job_dir/abstracts_testquery.xml");


# Merge the three files:
my $merge = "cat $job_dir/abstracts_set1.xml $job_dir/abstracts_set2.xml $job_dir/abstracts_testquery.xml $job_dir/abstracts_test.xml";
### Will complain if one of the test set options are missing (e.g. if only a search term is used), but it will still work:
system("$merge > $job_dir/abstracts.xml");



#########


# sleep 1 while ( !(-e "./$job_dir/abstracts.xml") );
if ($verbose eq "t") {print "<p>Extracting texts from xml...</p>\n"};

my %pmids_extracted;
my %titles;
my %pubyear;

open IN, "<", "$job_dir/abstracts.xml" or die $!;
$line = <IN>;
while (defined $line) {
	chomp $line;
	my $title;
	my $abstract;
	my $PMID;
	my $language;
	if ($line eq "<PubmedArticle>") {
		while (defined $line and $line ne "</PubmedArticle>") {
			if ($line =~ m/<ArticleTitle>(.+)<\/ArticleTitle>/) {
				$title = $1;
				$title =~ s/[^a-zA-Z0-9 ]/ /g
			}
			if ($line =~ m/^\s\s\s\s\s\s\s\s<PMID Version=".+">(.+)<\/PMID>/) {
				$PMID = $1;
			}
			if ($line =~ m/<AbstractText.*>(.+)<\/AbstractText.*>/) {
				$abstract .= " $1";
				$abstract =~ s/-/ /;
				$abstract =~ s/:/ /;
				$abstract =~ s/\*/ /;
			}
			if ($line =~ m/<Language>(.*)<\/Language>/) {
				$language .= lc "$1"
			}
			if ($line =~ m/<Journal>/) {
				while (defined $line) {
					if ($line =~ m/<\/Journal>/) {
						last;
					}
					if ($line =~ m/<Year>(.*)<\/Year>/) {
						$pubyear{$PMID} = $1;
					}
					$line = <IN>;
					chomp $line;
				}
			}	
			$line = <IN>;
			chomp $line;
		}
		if (defined $abstract and $PMID ne "" and $abstract ne "" and $language eq "eng" and exists $index{$PMID}) {
			if ($index{$PMID} == 1 or $index{$PMID} == 2) {
				open OUT, ">", "$job_dir/training_corpus/$PMID.txt" or die $!;
				print OUT lc "$title\n$abstract\n";
				close OUT;
				$pmids_extracted{$PMID} = 1;
			} 
			elsif ($index{$PMID} == 3) {
				open OUT, ">", "$job_dir/test_corpus/$PMID.txt" or die $!;
				print OUT lc "$title\n$abstract\n";
				close OUT;
				$pmids_extracted{$PMID} = 1;
			} 
			$titles{$PMID} = $title;
		}
		# elsif ($PMID ne "") {
		# 	print "$PMID\t$language\t";
		# 	if (exists $index{$PMID}) {
		# 		print "iy\t";
		# 	}
		# 	else {
		# 		print "in\t";
		# 	}
		# 	if (defined $abstract and $abstract ne "") {
		# 		print "ay\n";
		# 	}
		# 	else {
		# 		print "an\n";
		# 	}
		# }
	}
	$line = <IN>;
}
close IN;

my %pmid_warning;

if ($verbose eq "t") {print "<p>Printing files...</p>\n"};

open OUT, ">", "$job_dir/index_new.txt" or die $!;
open OUTA, ">", "$job_dir/article_info.txt" or die $!;
foreach my $key (keys %index) {
	if (exists $pmids_extracted{$key}) {
		if ($index{$key} == 1 or $index{$key} == 2) {
			print OUT "$key\t$index{$key}\n";
		}
		if ($index{$key} == 3) {
			if (exists $pubyear{$key}) {
				print OUTA "$key\t$pubyear{$key}\t$titles{$key}\n";
			}
			else {
				print OUTA "$key\tN/A\t$titles{$key}\n";
			}
		}
	}
	else {
		$pmid_warning{$key} = 1;
	}
}
close OUT;
close OUTA;

my $size_warning = keys %pmid_warning;

if ($size_warning > 0) {
	print "<p><b>Warning!</b> $size_warning out of ", scalar @pmids, " submitted PubMed ID(s) were not processed because of either A) not being found, B) abstract unavailable or C) no abstract in English.</p>  <p>\nThe following PubMed ID(s) were not processed:</p><p>" if $verbose eq "t";
        $warns .= "<p><b>Warning! $size_warning out of ". scalar @pmids. " submitted PubMed ID(s) were not processed because of either A) not being found, B) abstract unavailable or C) no abstract in English.</p>  <p>\nThe following PubMed ID(s) were not processed:</p><p>";
	foreach my $key (sort keys %pmid_warning) {
		print "$key " if $verbose eq "t";
                $warns .= "$key ";
	}
	print "</p>\n" if $verbose eq "t";
        $warns .= "</p>\n";
}


#print "This is the end\n";

#my $job_dir="/Users/krdav/Dropbox/bioreader/bioreader-31305-1447598141"; # while bioperl does not work
## Call R script
# my $cmd = "/usr/cbs/bio/src/bioreader-1.1/R/R-3.2.2/bin/Rscript /usr/cbs/bio/src/bioreader-1.1/classify_articles_parallel.R -i ";
my $cmd = "/usr/cbs/bio/src/bioreader-1.1/R/R-3.2.2/bin/Rscript /usr/cbs/bio/src/bioreader-1.1/classify_articles.R -i ";
#my $cmd = "Rscript classify_articles.R -i ";
my $wd = getcwd;
#$cmd .= $wd;
$cmd .= "$job_dir";
#if ($verbose eq "t") {print "<p>Calling R script with input parameters:\n - i $job_dir\n -t $term\n -e $email\n -v $verbose\n </p>"};
#print "<p>\n$cmd\n</p>";
if ($verbose eq "t") {print "<p>Building classifiers...</p>\n"};

# Print warnings:
if ($warns ne '') {
    print "<p>There was <a href=\"#warnings\">warnings</a> during the run.</p>\n";
    $warns = "<h4 id=\"warnings\">Warnings</h4>\n".$warns;
}
open OUT, ">", "$job_dir/warnings.txt" or die $!;
print OUT $warns;
close OUT;

system("$cmd > $job_dir/classify_articles.log");
system("/usr/cbs/bio/src/bioreader-1.1/main_js.pl $job_dir");


