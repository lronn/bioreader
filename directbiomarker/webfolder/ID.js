$(document).ready(function(){
    /* The following code is executed once the DOM is loaded */

    /* This flag will prevent multiple comment submits: */
    var working = false;

    /* Insert comments */
    $('#addCommentForm').submit(function(e){

        e.preventDefault();
        if(working) return false;

        working = true;
        $('#subcom').html('Working..');
        $('span.error').remove();

        /* Sending the form fileds to subcomment.php: */
        var data = $(this).serialize() + '&formSubmit=true';

       $.ajax({
        type: 'POST',
        url: 'subcomment.php',
        data: data
        })
            .done(function(html) {
                $('#eventdiv').prepend( html );
                working = false;
                $('#subcom').html('Submit');
            })
            .fail(function() {
                alert('error');
            });
    });


    /* Make vote and update events */
    $('#makeVoteForm').submit(function(e){

        e.preventDefault();
        if(working) return false;

        working = true;
        $('#subcom').html('Working..');
        $('span.error').remove();

        /* Sending the form fileds to subcomment.php: */
        var data = $(this).serialize() + '&formSubmit=true';

       $.ajax({
        type: 'POST',
        url: 'subvote.php',
        data: data
        })
            .done(function(html) {
                $('#eventdiv').prepend( html );
                working = false;
                $('#subcom').html('Submit');
            })
            .fail(function() {
                alert('error');
            });
    });

 
    /* Delete comments */
    $(document).on('click', '.deleteEventPic', function(e){
        e.preventDefault();
        if(working) return false;

        working = true;
        $('#subcom').html('Working..');
        $('span.error').remove();

        /* Sending the form fileds to delcomments.php: */
        var eventid = $(this).attr('name');
        var data = 'delete=' + eventid;
        //alert(data);

       $.ajax({
        type: 'POST',
        url: 'delcomment.php',
        data: data
        })
            .done(function(html) {
            	// If fail (on trying to delete something that does not belong to the user) break:
            	if (html == "Fail") {
            		alert('Error');
            		$('#subcom').html('Submit');
            		working = false;
            		return;
            	} 
                var pid = '#eventid'+eventid;
                //alert(pid);
                $(pid).hide();
                working = false;
                $('#subcom').html('Submit');
            })
            .fail(function() {
                alert('error');
            });
    });

});





$(document).ready(function(){
	$('#opnonProt, #excludeProt').prop( 'checked', function( i, val ) {
	    if (val) {
	        $('.ABcheck').prop({
	            disabled: true
	        });
	    }
	});

	$('#opnonProt, #excludeProt').click(function() {
	    $('.ABcheck').prop({
	        disabled: true
	    });
	    $('.ABcheck').prop({
	        checked: false
	    });
	});

	$('#includeProt').click(function() {
	    $('.ABcheck').prop({
	        disabled: false
	    });
	    /*
		If antibody vote is enabled then first AB on list is choosen:
	    $('#ABCheckboxID1').prop({
	        checked: true
	    });
		*/
	});

	$(document).on('mouseover', '.labelpopover', function () {
        $('[data-toggle="popover"]').popover();
    } );

});


$(document).on('mouseover', '.listheaderpopover', function () {
        $('[data-toggle="popover"]').popover();
} );



