# BioReader README #

This readme documents the use of the BioReader perl wrapper and R script

### Scripts ###

wrapper.pl: 
* imports abstracts by reading the tab separated file index.txt, which contains PubMed IDs and the class of the articles (1=positive, 2=negative, 3=to-be-classified
* calls NCBI using EUtils, and downloads the abstracts
* creates folder "corpus" containing the abstracts as txt files, and a number of parameter files for the R script
* calls the R script classify_articles.R

classify_articles.R
* trains machine learning algorithms to differentiate between the positive and negative articles class
* applies best performing algorithm to the abstracts to be classified.

### How do I get set up? ###

To be described.

### Contribution guidelines ###

To be described

### Who do I talk to? ###

lro@cbs.dtu.dk
krdav@cbs.dtu.dk