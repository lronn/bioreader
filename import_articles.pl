#!/usr/bin/perl
use strict;
use Bio::DB::EUtilities;
use Getopt::Long;
use CGI qw(:standard); # Needed packages for the CGI interface
use CGI::Carp qw(warningsToBrowser fatalsToBrowser); # Used to display CGI and Perl error in the growser

#Init CGI and print the header
my $q = new CGI;
print $q->header;
print "HEELO\n<br>";
# Setup env and dirs
my $job_dir;
# GetOptions("dir=s"=>\$job_dir);
# mkdir("$job_dir/training_corpus");
# mkdir("$job_dir/test_corpus");
# mkdir("$job_dir/output");

#Create input file from CGI data

#Fetch parameters from the frontend
my $set_1 = $q->param("set_1");
my $set_2 = $q->param("set_2");
my $set_test = $q->param("set_test");
print "$set_1\n<br>$set_2\n<br>$set_test\n";

#Parse input data
$set_1 =~ s/\n/ /g;
my @set_1 = split(/\s/,$set_1);
print "set_1\n<br>";
foreach my $key (@set_1) {
	$key =~ s/ //g;
	print "$key ";
}
print "\n<br>set_1\n<br>";
$set_2 =~ s/\n/\s/g;
my @set_2 = split(/ /,$set_2);
foreach my $key (@set_2) {
	$key =~ s/ //g;
	print "$key ";
}
print "\n<br>set_test\n<br>";
$set_test =~ s/\n/ /g;
my @set_test = split(/\s/,$set_test);
foreach my $key (@set_test) {
	$key =~ s/ //g;
	print "$key ";
}
print "\n<br>";
#Error check on input
# die unless $set_1;
# die unless $set_2;
# die unless $set_test;

# exit;

#Run script
open IN, "<", "./$job_dir/index.txt" or die $!;
my $line = <IN>;
my @pmids;
my %index;
while (defined $line) {
	chomp $line;
	$line =~ m/(\d*)\t(\d)/;
	$index{$1} = $2;
	push @pmids, $1;
	# print "$line\n";
	$line = <IN>;
}
close IN;
 
my $factory = Bio::DB::EUtilities->new(-eutil   => 'efetch', -db => 'pubmed', -rettype => 'gb', -email => 'lro@cbs.dtu.dk', -id => \@pmids);

my $file = 'abstracts.xml';
$factory->get_Response(-file => $file);

my %pmids_extracted;

open IN, "<", "abstracts.xml" or die $!;
$line = <IN>;
while (defined $line) {
	chomp $line;
	my $title;
	my $abstract;
	my $PMID;
	my $language;
	
	if ($line eq "<PubmedArticle>") {
		while (defined $line and $line ne "</PubmedArticle>") {
			if ($line =~ m/<ArticleTitle>(.+)<\/ArticleTitle>/) {
				$title = $1;
			}
			if ($line =~ m/^\s\s\s\s\s\s\s\s<PMID Version=".+">(.+)<\/PMID>/) {
				$PMID = $1;
			}
			if ($line =~ m/<AbstractText.*>(.+)<\/AbstractText.*>/) {
				$abstract .= " $1";
				$abstract =~ s/-/ /;
				$abstract =~ s/:/ /;
				$abstract =~ s/\*/ /;
			}
			if ($line =~ m/<Language>(.*)<\/Language>/) {
				$language .= "$1"
			}			
			$line = <IN>;
			chomp $line;
		}
		if (defined $abstract and $PMID ne "" and $abstract ne "" and $language eq "eng") {
			if ($index{$PMID} == 1 or $index{$PMID} == 2) {
				open OUT, ">", "./$job_dir/training_corpus/$PMID.txt" or die $!;
				print OUT lc "$title\n$abstract\n";
				close OUT;
				$pmids_extracted{$PMID} = 1;
			} 
			elsif ($index{$PMID} == 3) {
				open OUT, ">", "./$job_dir/test_corpus/$PMID.txt" or die $!;
				print OUT lc "$title\n$abstract\n";
				close OUT;
				$pmids_extracted{$PMID} = 1;
			} 
			
		}
	}
	$line = <IN>;
}
close IN;

my %pmid_warning;

open OUT, ">", "./$job_dir/index_new.txt" or die $!;
foreach my $key (keys %index) {
	if (exists $pmids_extracted{$key}) {
		if ($index{$key} == 1 or $index{$key} == 2) {
			print OUT "$key\t$index{$key}\n";
		}
	}
	else {
		$pmid_warning{$key} = 1;
	}
}
close OUT;

my $size_warning = keys %pmid_warning;

if ($size_warning > 0) {
	print "Warning: $size_warning out of ", scalar @pmids, " submitted PubMed ID(s) were not found or the abstract was unavailable or not in English.\n\nThe following PubMed ID(s) were not processed: ";
	foreach my $key (sort keys %pmid_warning) {
		print "$key ";
	}
	print "\n";
}


## Call R script

my $cmd = "Rscript classify_articles.R -i ";
$cmd .= $job_dir;
system($cmd)