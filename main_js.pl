#!/tools/bin/perl
use strict;
use warnings;




#my $fdir = '/usr/opt/www/pub/CBS/services/BioReader-1.2/tmp/bioreader-test/';
#my $sdir = '/services/BioReader-1.2/tmp/bioreader-test/';

my $fdir = "$ARGV[0]";
my $sdir = $1 if $fdir =~ /(\/services\/BioReader.*)/;
#print "$fdir\n";
#print "$sdir\n";




my @met;
my $fmet = $fdir.'/metrics.tab';
open(IN, "<", $fmet);
while (defined(my $line = <IN>)) {
    chomp($line);
    my @el = split('\t', $line);
    push(@met, @el);
}
close IN;



my $metrics = qq{
<table id="metrics_table" width="30%">
<thead>
<tr>
<th style="text-align:left;">
</th>
<th style="text-align:left;">
Positive
</th>
<th style="text-align:left;">
Negative
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;">
Precision
</td>
<td style="text-align:left;">
$met[0]
</td>
<td style="text-align:left;">
$met[1]
</td>
</tr>
<tr>
<td style="text-align:left;">
Recall
</td>
<td style="text-align:left;">
$met[2]
</td>
<td style="text-align:left;">
$met[3]
</td>
</tr>
<tr>
<td style="text-align:left;">
F score
</td>
<td style="text-align:left;">
$met[4]
</td>
<td style="text-align:left;">
$met[5]
</td>
</tr>
<tr>
<td style="text-align:left;">
Training set size
</td>
<td style="text-align:left;">
$met[6]
</td>
<td style="text-align:left;">
$met[7]
</td>
</tr>
<tr>
<td style="text-align:left;">
Algorithm
</td>
<td style="text-align:left;">
$met[8]
</td>
<td style="text-align:left;">
</td>
</tr>
</tbody>
</table>
</div>
};








my $pos;
my $fpos = $fdir.'/pos.tab';
#print "$fpos\n";

open(IN, "<", $fpos); 
while (defined(my $line = <IN>)) {
    chomp($line);    
    #print "$line\n";
    my @el = split('\t', $line);
    #print "******\n";
    #print "$el[0]\n";
    #print "$el[1]\n";
    #print "$el[2]\n";
    $el[0] =~ s/\\\"/\"/g;
    #print "$el[0]\n";
    $pos .= qq{
	        <tr>
	            <td>$el[0]</td>
	            <td>$el[1]</td>
	            <td>$el[2]</td>
	        </tr>};

}


$pos .= qq{
    </tbody>
</table>
</div>
};
close IN;









my $neg;

my $fneg = $fdir.'/neg.tab';

open(IN, "<", $fneg);
while (defined(my $line = <IN>)) {
    chomp($line);
    #print "$line\n";
    my @el = split('\t', $line);
    #print "******\n";
    #print "$el[0]\n";
    #print "$el[1]\n";
    #print "$el[2]\n";
    $el[0] =~ s/\\\"/\"/g;
    $neg .= qq{
                <tr>
                    <td>$el[0]</td>
                    <td>$el[1]</td>
                    <td>$el[2]</td>
                </tr>};

}


$neg .= qq{
    </tbody>
</table>
</div>
<br>
<br>
};
close IN;




my $space = '';


my $pic_path1 = $sdir.'/wordcloud.png';
my $pic_path2 = $sdir.'/pca.png';
my $pics;
$pics = qq{
<h4 id="class_term_h">Class signatures</4>
<div class="row">
    <div class="col-sm-6">

<div id="workcloud">
<center>
<img src="$pic_path1" width="90%">
</center>
</div>

    </div>
    <div class="col-sm-6">

    <div id="pca">
    <center>
    <img src="$pic_path2" width="90%">
    </center>
    </div>

    </div>
</div>
};




my $mstart;
$mstart .= qq{
<div id="div_metrics">
<h4 id="metrics_h">Performance metrics</h4>
};



#Article link
#Year of publication
#Score

my $pstart;
$pstart .= qq{
<div id="class-i-classification">
<h4 id="class_i_h">Class I classification</h4>
<table id="pos_table" class="display">
    <thead>
        <tr>
            <th><p>Article link</p></th>
            <th><p>Year of publication</p></th>
            <th><p>Score</p></th>
        </tr>
    </thead>
    <tbody>};


my $nstart;
$nstart .= qq{
<h4 id="class_ii_h">Class II classification</h4>
<div id="class-ii-classification">
<table id="neg_table" class="display">
    <thead>
        <tr>
            <th><p>Article link</p></th>
            <th><p>Year of publication</p></th>
            <th><p>Score</p></th>
        </tr>
    </thead>
    <tbody>};


# Get warnings:
open IN, "<", "$fdir/warnings.txt" or die $!;
my $warns = join("", <IN>);
close IN;


my $html;
$html = $mstart.$metrics.$pics.$pstart.$pos.$nstart.$neg."<br><br>".$warns;


print "$html\n";
__END__












