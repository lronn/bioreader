#!/usr/bin/perl

use DBI;
use CGI::Lite;
use strict;
use warnings;


############################
### Database connections ###
############################

sub db_connect {
	my $databaseName = 'krdav_private';
	my $dbuser = 'krdav';
	my $dbpassword = 'uUlZSoQm';

	# Make connection to server:
	my $dbc = DBI->connect("DBI:mysql:database=$databaseName;host=mysql.cbs.dtu.dk", $dbuser, $dbpassword, 
		{RaiseError => 1, AutoCommit => 1});
	return ($dbc);
}

my $dbh = db_connect ();





###########################
### Create FK from main ###
###########################

my $dbh_main = $dbh->prepare ("SELECT * FROM biomarker_main") or die ("Could not prepare statement: " . $dbh->errstr);
$dbh_main->execute ();


my %there;
while (my $main = $dbh_main->fetchrow_hashref ()) {
	unless (exists $there{$main->{'ENSG_ID'}}) {
		$there{$main->{'ENSG_ID'}} = $main->{'auto'};
	} else {
		die "There seems to be a duplicate ID in the database please check this out! ID = $main->{'ENSG_ID'}";
	}
}





my $commentsUP = $dbh->prepare("UPDATE org_names SET name = ? WHERE orgid = ?");
# DEL = DELETE
my $commentsDEL = $dbh->prepare("DELETE FROM biomarker_main WHERE orgid = ?");

# IN = INSERT
# This statement handles both new insert and update. If there is a duplicate key,
# the whole row is updated:
my $commentsIN = $dbh->prepare("INSERT INTO biomarker_eventlog (
FK_autoID,
user_name,
eventid,
comment) 
VALUES (?, ?, ?, ?)");

#$commentsIN->execute("222", "Mig", "Dig", "DM", "PHD", "1", '00000000');
#$commentsUP->execute("Purple", "Lilla", "Purple", "worker", "0", "2014.05.13", "77522");
#$commentsUP->execute("TheBeast", "Purple", "Lilla", "Purple", "worker", '00000000', "666");



#Eventids:
#1. Comment
#2. Vote yes
#3. Vote no
#4. Antibody insert
#5. Antibody remove



my $filename = 'comments_table_DB.csv';

open ("IN", "<", $filename) or die $!;
my $header = <IN>;

#my $len = 0;
#my $prelen = 0;
#my $count = 0;
while (defined(my $line = <IN>)) {
	chomp $line;
	my @el = split("\t", $line);
	#print "$el[-1]\n";
	next if $el[-1] eq "na";
	#$len = @el;
	#print join("*\n*",@el)."\n" if $len != $prelen;
	#print "current $len and previous $prelen count = $count\n" if $len != $prelen;
	#$prelen = $len;
	#$count++;
	# The information array is given twice: First if
	# new entry, second if duplicate key and update:
	if (exists $there{$el[0]}) {
		$commentsIN->execute($there{$el[0]}, $el[1], 1, $el[2]);	
	} else {
		die "Comment with no connection to ENSG_ID. Please correct this.\n";
	}
}
close IN;


__END__