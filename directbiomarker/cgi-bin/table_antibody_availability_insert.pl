#!/usr/bin/perl

use DBI;
use CGI::Lite;
use strict;
use warnings;


############################
### Database connections ###
############################

sub db_connect {
	my $databaseName = 'krdav_private';
	my $dbuser = 'krdav';
	my $dbpassword = 'uUlZSoQm';

	# Make connection to server:
	my $dbc = DBI->connect("DBI:mysql:database=$databaseName;host=mysql.cbs.dtu.dk", $dbuser, $dbpassword, 
		{RaiseError => 1, AutoCommit => 1});
	return ($dbc);
}


my $dbh = db_connect ();


my $antiUP = $dbh->prepare("UPDATE biomarker_antibodies SET availability=? WHERE antiID=?");



my $filename = 'aval_multiple_name_resolved.txt';

open (IN, "<", $filename) or die $!;
my $head = <IN>;
#print "something\n";
#print $head;



while (defined(my $line = <IN>)) {
	my @tmp = split(' ', $line);
	my $id = substr($tmp[0], 3,);
	my $aval = $tmp[-1]+1;

	#print "id=$id aval=$aval\n";
	$antiUP->execute($aval, $id);
}
close IN;





__END__