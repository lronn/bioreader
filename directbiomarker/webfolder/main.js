$(document).ready(function() {
    $('#marker_main').dataTable( {
    	'bAutoWidth': true,
    	'iDisplayLength': 25,
    	'aLengthMenu': [[25, 50, 100, -1], [25, 50, 100, 'All']],


        dom: 'C<\"clear\">lfrtip',
        columnDefs: [
            { visible: false, targets: [4,12,13,14,15,16,17,18,19,20,21,22,23,24,25] } // Remove 5,8,9,10,11 when done!!!
        ],
       
        colVis: {
        	'align': 'right',
        	restore: 'Restore',
            activate: 'click',
            'showAll': 'Show all',
            'aiExclude': [ 0 ],
              'label': function ( index, title, th ) {
              // To enable the use of <span> or popover in the <th>:	
              if (title.indexOf('<span') != -1) {
              	var end = title.indexOf('<span');
              	return (index+1)+'. '+title.slice(3,end);
              } else if (title.indexOf('>') != -1) {
              	var end = title.indexOf('>');
              	return (index+1)+'. '+title.slice(end+1, -1);
              } else {
              	var end = title.indexOf('</p>');
              	return (index+1)+'. '+title.slice(3,end);
              };


            }
        }
    } );
	var table = $('#marker_main').DataTable();
    new $.fn.dataTable.FixedHeader( table );

    $(document).on('mouseover', '.listheaderpopover', function () {
        $('[data-toggle="popover"]').popover();
    } );

} );


function listToRegex (e) {
	var regstr = e;
	var regstr = e.split(/[,;:\s]+/).join("|");
	regstr = regstr.replace(/(^\|+)/, '');
	regstr = regstr.replace(/(\|+$)/, '');
	return regstr;
}


function filterGlobal () {
	if ($('#global_regex').prop('checked')) {
	    $('#marker_main').DataTable().search(
	    	// Replace whitespace, comma, colon or semicolon with REGEX OR symbol:
	    	listToRegex($('#global_filter').val()),
	        true,
	        false
	    ).draw();
	} else {
		$('#marker_main').DataTable().search(
	    	// Replace whitespace, comma, colon or semicolon with REGEX OR symbol:
	    	listToRegex($('#global_filter').val()),
	        false,
	        true
	    ).draw();
	}
}
 

$(document).ready(function() {
    $('#marker_main').dataTable();
 
    $('input.global_filter').on( 'keyup click', function () {
        filterGlobal();
    } );
} );


$(document).ready(function() {
	$('#statgreen').popover();
	$('#statred').popover();
	$('#statyellow').popover();
} );





/*
$(document).ready(function() {
    $('#marker_main').dataTable();
     
    $('#marker_main tbody').on('click', 'th', function () {
        $('#statgreen2').popover();
    } );
} );

// Passing in options
var options = { optionName : optionValue };
$('a.popup').popup(options);


$(document).ready(function() {
	$('#movingSearch').prependTo('#marker_main_filter');
} );
*/


