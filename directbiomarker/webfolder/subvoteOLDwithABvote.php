<?php

/*
/	This array is going to be populated with either
/	the data that was sent to the script, or the
/	error messages:
/*/

$servername = "mysql.cbs.dtu.dk";
$username = "krdav";
$password = "uUlZSoQm";
$dbname = "krdav_private";

/*
Put in some data to $_POST to simulate a comment

var_dump($_POST);
echo $_POST["FK_autoID"];
echo "More text";
exit();
$_POST["FK_autoID"] = '417';
$_POST["user_name"] = 'krdav';
$_POST["pcomment"] = 'This is a test';
*/

# Username after login:
$login = getenv('REMOTE_USER');
#print "$login\n";



$arr = array(
	'FK_autoID' => $_POST["FK_autoID"],
	'user_name' => $login,
	'eventid' => $_POST["RadioProt"],
	//'comment' => $_POST["pcomment"]
);

$date = new DateTime('Europe/Berlin');
$dt = $date->format('Y-m-d H:i:s');
//print "$dt\n";
$arr['datetime'] = $dt;

$count = 1;
while ($count < 100) {
	if ($arr['eventid'] == 2 and $_POST["ABcheckName".$count]) {

		$arr['antibody'] = $_POST["ABcheckName".$count];
	} else {
		$arr['antibody'] = '';
	}


	$validates = 1;

	if ($validates) {
	    /* Everything is OK, insert to database: */

		/*
	    */
	    $mysqli = new mysqli($servername, $username, $password, $dbname);

	    if (mysqli_connect_errno()) {
	    	printf("Connect failed: %s\n", mysqli_connect_error());
	    	exit();
		}
		$mysqli->query("	INSERT INTO biomarker_eventlog(FK_autoID, user_name, eventid, antiID, datetime)
	                    VALUES (
	                        '".$arr['FK_autoID']."',
	                        '".$arr['user_name']."',
	                        '".$arr['eventid']."',
	                        '".$arr['antibody']."',
	                        '".$arr['datetime']."'
	                    )");

	    $evID = $mysqli->insert_id;

	    $mysqli->close();

	    //print "<p id=\"eventid".$evID."\">".$arr['datetime']." - <b>Comment from ".$arr['user_name'].":</b> ".$arr['comment']."\n<img src=\"delete.png\" height=\"15\" width=\"15\" class=\"deleteEventPic\" name=\"".$evID."\"></p>";
	    //print var_dump($_POST);
	    if ($arr['eventid'] == 2 and $_POST["ABcheckName".$count]) {
	    	print "<p id=\"eventid".$evID."\">".$arr['datetime']." - <b>".$arr['user_name']." included this protein with antibody ".$arr['antibody']."</b>\n<img src=\"delete.png\" height=\"15\" width=\"15\" class=\"deleteEventPic\" name=\"".$evID."\"></p>";
	    // Not works!!! Also triggered if voting for only one out of two
	    } elseif ($arr['eventid'] == 2 and !$_POST["ABcheckName".$count]) {
	    	print "<p id=\"eventid".$evID."\">".$arr['datetime']." - <b>".$arr['user_name']." included this protein with no antibody</b>\n<img src=\"delete.png\" height=\"15\" width=\"15\" class=\"deleteEventPic\" name=\"".$evID."\"></p>";
	    } elseif ($arr['eventid'] == 3) {
	    	print "<p id=\"eventid".$evID."\">".$arr['datetime']." - <b>".$arr['user_name']." excluded this protein</b>\n<img src=\"delete.png\" height=\"15\" width=\"15\" class=\"deleteEventPic\" name=\"".$evID."\"></p>";
	    } elseif ($arr['eventid'] == 4) {
	    	print "<p id=\"eventid".$evID."\">".$arr['datetime']." - <b>".$arr['user_name']." had no opinion on this protein</b>\n<img src=\"delete.png\" height=\"15\" width=\"15\" class=\"deleteEventPic\" name=\"".$evID."\"></p>";
	    }


	    //print $arr['antibody'];

	} else {
	    print "Fail";
	}

	if ($arr['eventid'] == 3 or $arr['eventid'] == 4) {
		break;
	} elseif ($arr['eventid'] == 2 and !$_POST["ABcheckName".$count]) {
		break;
	}

	$count++;
} 

?>
