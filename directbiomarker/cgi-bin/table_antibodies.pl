#!/usr/bin/perl

use DBI;
use CGI::Lite;
use strict;
use warnings;


############################
### Database connections ###
############################

sub db_connect {
	my $databaseName = 'krdav_private';
	my $dbuser = 'krdav';
	my $dbpassword = 'uUlZSoQm';

	# Make connection to server:
	my $dbc = DBI->connect("DBI:mysql:database=$databaseName;host=mysql.cbs.dtu.dk", $dbuser, $dbpassword, 
		{RaiseError => 1, AutoCommit => 1});
	return ($dbc);
}


my $dbh = db_connect ();



########################################
### Find duplicates in given dataset ###
########################################

my $filename = 'main_table_DB.csv';

open ("IN", "<", $filename) or die $!;
my $header = <IN>;


my %seen;
my $dup = 0;
while (defined(my $line = <IN>)) {
	chomp $line;
	my @el = split("\t", $line);
	my $rmindex = $#el-3;
	my @anti_info = splice (@el, $rmindex, 3);

	if ($anti_info[0] ne 'NS') {
		#print "$anti_info[0]\n";
		my @ids = split(";", $anti_info[0]);
		#my @vols = split(";", $anti_info[1]);
		#my @concs = split(";", $anti_info[2]);

		#my $i = 0;
		foreach my $k (@ids) {
			unless (exists $seen{$k}) {
				#print "$k\n";
				$seen{$k}++;	
				next;
				print "'$k' is duplicated.\n";
    			$dup++;
			}

			#$i++;
		}
	}
}
close IN;
die "Please remove duplicates" if $dup;




###########################
### Create FK from main ###
###########################

my $dbh_main = $dbh->prepare ("SELECT * FROM biomarker_main") or die ("Could not prepare statement: " . $dbh->errstr);
$dbh_main->execute ();


my %there;
while (my $main = $dbh_main->fetchrow_hashref ()) {
	unless (exists $there{$main->{'ENSG_ID'}}) {
		$there{$main->{'ENSG_ID'}} = $main->{'auto'};
	} else {
		die "There seems to be a duplicate ID in the database please check this out! ID = $main->{'ENSG_ID'}";
	}
}







my $antiIN = $dbh->prepare("INSERT INTO biomarker_antibodies (
FK_autoID,
antiID,
anti_vol_ml,
anti_conc_mg_ml,
used) 
VALUES (?, ?, ?, ?, ?)");




open ("IN", "<", $filename) or die $!;
my $newheader = <IN>;



while (defined(my $line = <IN>)) {
	chomp $line;
	my @el = split("\t", $line);
	my $rmindex = $#el-3;
	my @anti_info = splice (@el, $rmindex, 3);

	my $ENSG_ID = $el[0];
	if ($anti_info[0] ne 'NS') {
		#print "$anti_info[0]\n";
		my @ids = split(";", $anti_info[0]);
		my @vols = split(";", $anti_info[1]);
		my @concs = split(";", $anti_info[2]);

		my $i = 0;
		foreach my $k (@ids) {
			#print "$there{$ENSG_ID}, $k, $vols[$i], $concs[$i]\n";
			$antiIN->execute($there{$ENSG_ID}, $k, $vols[$i], $concs[$i], 'yes');

			$i++;
		}
	}
}
close IN;




__END__