<?php
# Always include this file
include "cbs_std.php";
# Create a html header, give the title
standard_head("CBS Projects","","
<!-- Stylesheet -->
<link rel='stylesheet' type='text/css' href='/projects/directbiomarker/colVis.css' />
<link rel='stylesheet' type='text/css' href='/projects/directbiomarker/biomarker.css' />

<link href='styles.css' rel='stylesheet' type='text/css' />


<!-- DataTables CSS -->
<link rel='stylesheet' type='text/css' href='//cdn.datatables.net/1.10.3/css/jquery.dataTables.css'>
<!-- <link rel='stylesheet' type='text/css' href='//cdn.datatables.net/colvis/1.1.1/css/dataTables.colVis.css'> -->
<link rel='stylesheet' type='text/css' href='//cdn.datatables.net/fixedheader/2.1.2/css/dataTables.fixedHeader.css'>
<link rel='stylesheet' type='text/css' href='//cdn.datatables.net/tabletools/2.2.3/css/dataTables.tableTools.css'>


<!-- jQuery -->
<script type='text/javascript' charset='utf8' src='//code.jquery.com/jquery-1.10.2.min.js'></script>


<!-- DataTables -->
<script type='text/javascript' charset='utf8' src='//cdn.datatables.net/1.10.3/js/jquery.dataTables.js'></script>
<script type='text/javascript' charset='utf8' src='//cdn.datatables.net/colvis/1.1.1/js/dataTables.colVis.min.js'></script>
<script type='text/javascript' charset='utf8' src='//cdn.datatables.net/fixedheader/2.1.2/js/dataTables.fixedHeader.min.js'></script>
<script type='text/javascript' charset='utf8' src='//cdn.datatables.net/tabletools/2.2.3/js/dataTables.tableTools.min.js'></script>


<!-- Bootstrap -->
<!-- Latest compiled and minified CSS -->
<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css'>

<!-- Optional theme -->
<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css'>

<!-- Latest compiled and minified JavaScript -->
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js'></script>



<!-- Javascript -->
<script type='text/javascript' src='/projects/directbiomarker/ID.js'></script>




");

# Username after username:
$username = getenv('REMOTE_USER');
#print "$username\n";

# Notice that a standard CBS header has some css that overwrites part of mine.
#
# Create CBS menu. Format is:
# Keyword, Path, Name_of_this_page
# Keyword is the keyword for the menu color/area.
# Name_of_this_page is what this page is called in the hieraki
# Path format is a number of comma separated entries in parenthesis
# showing the path to this page; (services/,'CBS Prediction Servers')

standard_menu("CBSDS","(/projects,'CBS Projects'),(/projects/directbiomarker,'Biomarker')","Browse");
$ENSG_ID = $_GET["id"];


# Header
print "<h1>Detailed information about $ENSG_ID</h1>";

?>

<!-- INDHOLD -->


<hr>
<?php
# this works

print shell_exec("/usr/opt/www/cgi-bin/CBS/directbiomarker/ENSG.pl $ENSG_ID $username");
?>
<p>

<?php
# Displays a standard footer; two parameters:
# First a simple headline like: "GETTING HELP:"
# then a list of emails like this:
# "('Tech assist','Frank','frank@foo.net'),('Scient assist','Bent','bent@foo.net')"
standard_foot("Getting help","('Technical assistance','Peter Wad Sackett','pws@cbs.dtu.dk')");
?>
